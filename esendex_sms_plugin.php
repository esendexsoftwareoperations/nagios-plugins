#!/usr/bin/php
<?php
include('httpful.phar');
 
echo $argv[1];

function SendSMS($accountreference, $from,$username, $password, $recipient, $messagebody) {
	echo $messagebody;
	$messages = new SimpleXMLElement("<messages></messages>");
	$messages->addChild('accountreference',$accountreference);
	$messages->addChild('from',$from);
	$message = $messages->addChild('message');
	$message->addChild('to',$recipient);
	$message->addChild('body',$messagebody);

	$response = \Httpful\Request::post("http://api.internal.esendex.com/v1.0/messagedispatcher")
		->authenticateWith($username,$password)
		->body($messages->asXML())
		->sendsXml()
		->send();
	
	switch($response->code) {
		case 200:
		print "OK";
		exit(0);
		default:
		print "UNKNOWN";
		exit(1);
	} 
}
SendSMS($argv[1],$argv[2],$argv[3],$argv[4],$argv[5],$argv[6]);
?>
